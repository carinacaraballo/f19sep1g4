//This is the challenge question organizers can post
import java.util.Scanner;

public class Challenge
{
    private String challengeQuestion;
    private Date dueDate;
    
    public Challenge()
    {
        challengeQuestion = "DEFAULT";
        dueDate = new Date();
    }
    public Challenge(String q, Date due)
    {
        setChallengeQuestion(q);
        setDueDate(due);
    }
    public Challenge(Challenge copy)
    {
        setChallengeQuestion(copy.challengeQuestion);
        setDueDate(copy.dueDate);
    }
    //setters
    public void setChallengeQuestion(String cQ)
    {
/*        if(cQ == NULL){
            System.out.println("ERROR");
            System.exit(0);
        }
        else {*/
            challengeQuestion = cQ;
       // }
    }
    public void setDueDate(Date dD)
    {/*
        if(dD == NULL){
            System.out.println("ERROR");
            System.exit(0);
        }
        else {*/
            dueDate = new Date(dD);
       // }
    }
    //getters
    public String getChallengeQuestion()
    {
        return challengeQuestion;
    }
    public Date getDueDate()
    {
        return dueDate;
    }
    //equals
    public Boolean equals(Challenge other) 
    {
        return (challengeQuestion == other.challengeQuestion && dueDate.equals(other.dueDate));
    }
    public String toString()
    {
        return ("The question is :" + challengeQuestion +" due "+ dueDate.toString());
    }  
} 

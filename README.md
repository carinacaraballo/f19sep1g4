This system’s primary goal is creating a discussion based interface 
in which the organizer could post challenge questions and participants 
could sign in and see the challenge questions posted and choose to submit 
solutions for the questions available. The organizer could see how many 
participants attempted to solve the challenge. They could also choose to 
remove a challenge they previously posted. Participants will not be able 
to see the solution of other participants, but the system will tell the 
participant if they are right or wrong and regardless of the answer, the 
system will record the attempt. The participant could choose to answer 
again or they could move on. The administration is in charge of deciding 
who has access to the system by having the power to add and remove new 
users (a user could be a participant or an organizer). The administration 
could also see all problems, outcomes, and solutions posted on the page to
analyze how participants and organizers are using the system.

User Requirements
-Sign in/Sign out —Login
-Change password
-Post Challenge Question
-Post Solution
-Attempt Challenge
-See all posted challenges
-Attempt a Challenge
-Submit Solution
-See Past attempts
-Add user <<extends to>> Assign Role
-Remove user
-See Solution
-See outcomes/attempts
-Update Challenge


System Requirements
-Administrative rights to the Administrator
-Administrator > Organizer > Participant
-Users (Administrator, Organizer, and Participant) need to Login
-Challenge Questions, solution, and any outcomes are only available for two weeks after posting
-Verify Participant wants to attempt solution by showing them their outcome on the screen before
submitting
-Check if Participant solution is correct (if participantSol == challenge.solution) and return
“Correct Solution” or “Incorrect Attempt”
-Give Participant two more tries to get the challenge correct
-Return solution after third attempt
-Keep count of all attempts on challenges by all participants
-Keep participant attempts until third attempt, then delete them all
-Users are only allowed to change their passwords from user profile
-Administrator allowed to change roles of users
-Clear memory space that had been allocated to user
-System doesn’t allow an organizer to post a challenge already posted
-Allow multiple users to use interface at the same time.


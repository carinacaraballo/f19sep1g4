import  java.util.Scanner;

//this is the date class
//year could only be between 2000 and 2020
//day could only be between 1 and 31
//month could only be 1 to 12

public class Date
{
     private String month;
     private int day;
     private int year;
     
     //creating a default date constructor
     public Date() 
     {
         month = "January";
         day = 1;
         year = 2000;
     }
     public Date(String m, int d, int y)
     {
         setMonth(m);
         setDay(d);
         setYear(y);
     }
     public Date(int m, int d, int y)
     {
         setMonth(m); 
         setDay(d);
         setYear(y);
     }
     public Date(Date other)
     {
         month = other.month;
         day = other.day;
         year = other.year;
     }
     public void setMonth(String m)
     {
         if((m=="January"||m=="February")||(m== "March"||m=="April")||(m=="May"||m=="June")||(m=="July"||m=="August")||(m=="September"||m=="October")||(m=="November"||m=="December"))
             {
                 month = m; 
             }
         else {
             System.out.println("Error: Month not recognized.");
             System.exit(0);
         }
     }
     public void setMonth(int m)
     {
         if(m>0 && m<13) {
             month = monthToString(m);
         }
         else {
             System.out.println("Error: Month not recognized.");
             System.exit(0);
         }
     } 
     public String monthToString(int m)
     {  
         String error = "Error";
         if (m==1){
             return "January";}
         else { if (m==2){
                   return "February";}
                else { if (m==3){
                           return "March";}
                       else{ if (m==4){
                                 return "April";}
                             else { if (m==5) {
                                        return "May";}
                                    else { if (m==6) {
                                               return "June";}
                                           else{ if (m==7) {
                                                     return "July";}
                                                 else{ if (m==8) {
                                                           return "August";}
                                                       else{ if (m==9) {
                                                                 return "September";}
                                                             else{ if (m==10) {
                                                                       return "October";}
                                                                   else{ if (m==11){
                                                                             return "November";}
                                                                         else{ if(m==12){
                                                                                  return "December";}
                                                                               else{ 
                                                                                   return error;
                                                                                   //System.exit(0);} 
        }}}}}}}}}}}}
     }
     public void setDay(int d)
     { 
          if(month == "February"){
              if(d>0&&d<30){
                  day = d;
              }
              else{
                  System.out.println("Invalid day.");
                  System.exit(0);
              }
         }
         else{ if((month == "January" || month == "March") || (month == "May" || month == "July") || (month == "August" || month == "October") || month == "December"){
                   if ( d>0&&d<32){
                       day = d;
                   }
                   else { 
                       System.out.println("Invalid day.");
                       System.exit(0);
                   }
               }
               else{ if((month == "April"||month == "June")||(month == "September"||month == "November")){
                         if(d>0&&d<31){
                             day = d;
                         }
                         else {
                             System.out.println("Invalid day.");
                             System.exit(0);
                         }
                     }
                     else {
                         System.out.println("Error: Invalid day!");
                         System.exit(0);
                     }
               }
          }
     }
     public void setYear(int y)
     {
         if ( y> 2000 && y < 2025) { 
             year = y;
         }
         else {
             System.out.println("Error: Invalid Year.");
             System.exit(0);
         }
     } 
     public String getMonth()
     {
         return month;
     }
     public int getDay()
     {
         return day;
     }
     public int getYear()
     {
         return year;
     }

     //so now I have getters and setters 
     //still need a tostring method and an equals method
     public String toString()
     {
         return getMonth() + " " + getDay() +", "+  getYear();
     }
     public Boolean equals(Date other)
     {
         return (month == other.month && day == other.day && year == other.year);
     }
}

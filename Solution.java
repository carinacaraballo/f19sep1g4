import java.util.Scanner;

public class Solution extends Challenge 
{
    private String solution;
   

    public Solution(){
        solution = "DEFAULT: There isn't a solution posted.";
    }
 
    public Solution(String ques, Date due, String s)
    {
        super(ques, due);
        setSolution(s);
    }
    public Solution(Solution copy)
    {
        setSolution(copy.solution);
    }
    //private setters because for now lets not allow changing of solution
    private void setSolution(String s)
    {
        solution = s;
    }
    public String getSolution()
    {
        return solution;
    }
    public String toString()
    {
        return super.toString() + "and it's solution is: " + solution;
    }
    public Boolean equals(Solution other)
    {
        return (solution == other.solution);
    }
}

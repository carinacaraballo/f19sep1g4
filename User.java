import java.util.Scanner;

//This will be the user superclass

public class User {
    private String name, lname;
    private Date dateOfCreation;
    private String username;
    private String password;

    public User()
    {
        name = "NoName";
        lname = "NoLastName";
        dateOfCreation = new Date();
        username = name+lname;
        password = "default";
    }
    public User(String first, String last, Date doc){     
        name = first;
        lname = last;
        dateOfCreation = new Date(doc);
        username = name+lname;
        password = "default";
    }
    public User(User copy)
    {
        name = copy.name;
        lname = copy.lname;
        dateOfCreation = new Date(copy.dateOfCreation);
        username = name+lname;
        password = "default";
        
    }
    public String getName()
    {
        return name+lname;
    }
    public Date getDateOfCreation()
    {
        return dateOfCreation;
    }
    public void setFirstName(String fi)
    {
        name = fi;
    }
    public void setLastName(String la)
    {
        lname = la;
    }
    public void setName(String fir, String las)
    {
        name = fir;
        lname = las;
    }
    public void setDateOfCreation(Date dt)
    {
        dateOfCreation = new Date(dt);
    }
    public void resetPassword(String pw)
    {
        password = pw; 
    }
    public String toString()
    {
        return ("The user was created "+dateOfCreation.toString()+" and their full name is "+name+" "+lname+"." + name+"'s username is "+username);
    }
    public Boolean equals(User other)
    { 
        return (username.equals(other.username) && dateOfCreation.equals(other.dateOfCreation));
    }


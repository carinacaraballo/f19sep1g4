/* We have an attempt class that extends solution.
*It attempts a participant solution and returns if it is correct
*Also keeps a count of attempts
*/

import java.util.Scanner;

public class Attempt extends Solution
{
    private int count = 0;
    private String userInput;

    public Attempt()
    {
        super();
        count = 0;
        userInput = "None Entered";
    }
    public Attempt(String att)
    {
        super();
        userInput = att;
        count++;
    }
    public Boolean correct(String input)
    {
        return input.equals(getSolution());
    }
}


/*This is the Participant object
* By Carina C
*/

import java.util.Scanner;

public class Participant extends User
{
    private String role;//the role which I initialize in constructors
    
    //create all the constructors by which the participant could be created  
    public Participant()
    {
        super();
        role = "Participant";
        
    }
    public Participant(String name, String lname, Date doc, String r)
    {
        super(name, lname, doc);
        role = r;
    }
    public Participant(Participant other)
    {
        super(other);
        role = other.role;
    }
    //allow others to set the role of the participant
    public void setRole(String r)
    {
        role = r;
    }
    //also to get the role
    public String getRole()
    {
        return role;
    }
    //the toString method prints a summary of the participant 
    public String toString()
    {
        return (super.toString() + "and their role is: " + role);
    }
    //participant can submit solutions and this function will 
    //return the number of attempts
    public int submitSolution(String s)
    {
        Attempt check = new Attempt(s);
        return Attempt.getCount();
    }
}
